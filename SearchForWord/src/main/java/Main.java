import services.MSWordTextFinder;
import services.PDFTextFinder;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException, IndexOutOfBoundsException, FileNotFoundException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Scanner in = new Scanner(System.in);
        int choise;

        System.out.println("What file do you ant to search?");
        System.out.println("Press: 1 for PDF or 2 for DOCX");
        choise = in.nextInt();

        switch (choise) {
            case 1:
                System.out.println("Please enter the path of the file: ");
                String pathname = br.readLine();
                System.out.println("Please enter a word for search: ");
                String word = br.readLine();

                List<String> wordlist = new ArrayList<String>();
                wordlist.add(word);

                PDFTextFinder pdfTextFinder = new PDFTextFinder();
                pdfTextFinder.findTextFromPDFDocument(pathname, wordlist);

                break;

            case 2:
                System.out.println("Please enter the path of the file: ");
                String pathnameDOC = br.readLine();
                System.out.println("Please enter a word for search: ");
                String wordDOC = br.readLine();

                List<String> wordlistDOC = new ArrayList<String>();
                wordlistDOC.add(wordDOC);

                MSWordTextFinder wordTextFinder = new MSWordTextFinder();
                wordTextFinder.readTextFromDocDocument(pathnameDOC, wordlistDOC);

                break;
        }
    }
}
