package services;


import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PDFTextFinder extends SearchResults {

    public void findTextFromPDFDocument( String pathname, List<String> wordList) throws IOException, IndexOutOfBoundsException, FileNotFoundException {
        List<String> dwords = new ArrayList<>();
        List<Integer> pages = new ArrayList<>();

        try (PDDocument document = PDDocument.load(new File(pathname))) {
            document.getClass();
            if (!document.isEncrypted()) {
                PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                stripper.setSortByPosition(true);
                PDFTextStripper tStripper = new PDFTextStripper();

                for (String text : wordList) {
                    dwords.add(text);
                }

                for (int k=0; k<dwords.size(); k++) {
                    for (int i = 1; i <= document.getNumberOfPages(); i++) {
                        tStripper.setStartPage(i);
                        tStripper.setEndPage(i);
                        if (tStripper.getText(document).contains(dwords.get(k))) {
                            pages.add(i);
                        }
                    }
                }

                for (String dword : dwords) {
                    if (!(pages.size() == 0)) {
                        setFound(true);
                        System.out.println("The word: " + dword + ", Found: " + found + ", at page: " + pages);
                    } else {
                        setFound(false);
                        System.out.println("The word: " + dword + ", Found " + found);
                    }
                }
            }
        }
        catch(Exception exep){}
    }
}
